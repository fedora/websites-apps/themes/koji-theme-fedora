This package is a theme that makes Koji's web interface look nice and Fedora-y

It works by:

1. overriding parts of the /koji-static tree in the httpd config, replacing some of that content 
   with our own. These changes are done on the fedora Koji with the infra repo package koji-theme-fedora
2. We also have to run patched versions of the header and footer templates. These are in the template/
   directory here in this repo, and applied to Fedora Koji as a patch to the infra koji rpm package.

# Developing the Theme

There is a vagrant setup to allow quickly hacking on the theme.

Install Ansible, Vagrant and the vagrant-libvirt plugin from the official Fedora
repos:

    $ sudo dnf install ansible vagrant vagrant-libvirt vagrant-sshfs


Now, from within main directory (the one with the Vagrantfile in it) of your git
checkout of koji-theme-fedora, run the ``vagrant up`` command to provision your dev
environment:

    $ vagrant up

When this command is completed (it may take a while) you will be able go to
http://koji.test/koji in your browser on your host to see your running
koji test instance.

When you make changes, you will need to run the update and reload script to see the changes:

    $ vagrant ssh -c "kojitheme-reload"
